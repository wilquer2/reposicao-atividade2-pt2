#!/bin/bash

echo                                "Menu"

echo '==============================================================================='

echo "a) Selecione um diretorio"
sleep 1
echo "b) Crie um arquivo com N frases aleatória, sendo N informado pelo usuário"
sleep 1
echo "c) Exiba apenas os arquidos do diretorio"
sleep 1
echo "d) Exiba apenas os subdiretórios deste diretorio"
sleep 1
echo "e) Exiba apenas os executáveis deste diretório"
sleep 1
echo "f) Renomeie todos os arquivos deste diretório, colocando todas as letras em maiúsculas"
sleep 1
echo "g) Renomeie todos os arquivos deste diretório, colocando todas as letras em minúsculas"
sleep 1
echo "h) Renomeie todos os arquivos com extensão .txt para .docx"
sleep 1
echo "i) Remova todos os arquivos com 3 linhas ou mais"
sleep 1
echo "j) Sair"
sleep 1

